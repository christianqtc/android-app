package com.amazonaws.mobile.api;

//
//  CloudLogicAPIFactory.java
//
//
// Copyright 2017 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
//
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to 
// copy, distribute and modify it.
//
// Source code generated from template: aws-my-sample-app-android v0.19
//

/**
 * Produces instances of Cloud Logic API configuration.
 */
public class CloudLogicAPIFactory {

    private CloudLogicAPIFactory() {}

    /**
     * Gets the configured micro-service instances.
     * @return
     */
    public static CloudLogicAPIConfiguration[] getAPIs() {
        final CloudLogicAPIConfiguration[] apis = new CloudLogicAPIConfiguration[] {
                new CloudLogicAPIConfiguration("routes",
                                              "used to perform transactions on the routes table",
                                              "https://hzly4busx0.execute-api.us-west-1.amazonaws.com/Development",
                                              new String[] {
                                                  "/createRoute",
                                                  "/createRoute/123",
                                                  "/getRoutes",
                                                  "/getRoutes/123",
                                                  "/storeRoute",
                                                  "/storeRoute/123",
                                                  "/useRoute",
                                                  "/useRoute/123",
                                              },
                                              com.amazonaws.mobile.api.idhzly4busx0.RoutesMobileHubClient.class),
                new CloudLogicAPIConfiguration("biker",
                                              "used to manipulate database for a user in the biker table",
                                              "https://c0jz1wxof3.execute-api.us-west-1.amazonaws.com/Development",
                                              new String[] {
                                                  "/createUser",
                                                  "/createUser/123",
                                                  "/verifyUser",
                                                  "/verifyUser/123",
                                              },
                                              com.amazonaws.mobile.api.idc0jz1wxof3.BikerMobileHubClient.class),
        };

        return apis;
    }
}
